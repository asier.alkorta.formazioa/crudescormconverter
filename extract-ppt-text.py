#!/usr/bin/env python3

# python pip install ´libxml2 libxslt python-pptx

from pptx import Presentation

# Define the path to the PPT file
pptx_file = "06_Puesto_trabajo.pptx"

# Load the PPT presentation
presentation = Presentation(pptx_file)

# Initialize an empty string to store the extracted text
extracted_text = ""

# Iterate through slides and extract text from shapes and speaker notes
for slide in presentation.slides:
    for shape in slide.shapes:
        if shape.has_text_frame:
            for paragraph in shape.text_frame.paragraphs:
                for run in paragraph.runs:
                    extracted_text += run.text + " "
    # Check if there are speaker notes for the slide
    if slide.has_notes_slide:
        notes_slide = slide.notes_slide
        for shape in notes_slide.shapes:
            if shape.has_text_frame:
                for paragraph in shape.text_frame.paragraphs:
                    for run in paragraph.runs:
                        extracted_text += run.text + " "

# Print or save the extracted text
print(extracted_text)

# If you want to save the text to a text file, you can do:
# with open("output.txt", "w", encoding="utf-8") as text_file:
#     text_file.write(extracted_text)
