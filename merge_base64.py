#!/usr/bin/env python3

import os
import base64

# Directory where the .b64 files are located
folder_path = '.'

# Output text file to store the results
output_file = 'output.txt'

# Function to list .b64 files in the folder
def list_b64_files(folder_path):
    b64_files = []
    for filename in os.listdir(folder_path):
        if filename.endswith('.b64'):
            b64_files.append(filename)
    return b64_files

# Function to decode and save content to a text file
def create_text_file(filename, content):
    with open(filename, 'wb') as text_file:
        text_file.write(content)

# List .b64 files in the folder
b64_files = list_b64_files(folder_path)

# Process each .b64 file
with open(output_file, 'w') as output:
    for filename in b64_files:
        with open(os.path.join(folder_path, filename), 'rb') as b64_file:
            encoded_content = b64_file.read()
            decoded_content = base64.b64decode(encoded_content)
            create_text_file(filename + '.txt', decoded_content)
            output.write(f"File: {filename}\n")
            output.write(f"Content:\n{decoded_content.decode('utf-8')}\n")
            output.write("\n")

print(f".b64 files have been decoded and text files have been created. Results are stored in {output_file}.")
