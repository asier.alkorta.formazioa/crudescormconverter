#!/usr/bin/env python3



def pdfToHtml(file):
    print("converting to ")
    import subprocess
    import os
    
    # Define the command to run as a list of strings
    command = ["pdftohtml", "-c", file]

    # Run the command
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, cwd=os.getcwd())

    # Capture the standard output and error
    stdout, stderr = process.communicate()

    # Check the return code to see if the command succeeded
    return_code = process.returncode    
  
  
def delete_html_files(folder_path, extension):
    import os
    import shutil
    
    try:
        # List all files in the specified folder
        files = os.listdir(folder_path)

        # Iterate through the files and delete HTML files
        for file in files:
            if file.endswith(extension):
                file_path = os.path.join(folder_path, file)
                os.remove(file_path)
                #print(f"Deleted: {file_path}")

        print("HTML files have been deleted from the folder.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        
def clean():
    print("Cleaning...")
    #delete_html_files(".",".html")
    delete_html_files_except_selected(".",[".pdf","-clean.html","divs.html"])
    delete_html_files(".",".png")


def delete_html_files_except_selected(folder_path, files_to_keep):
    import os
    try:
        # List all files in the specified folder
        files = os.listdir(folder_path)

        # Iterate through the files and delete HTML files that are not in the array
        for file in files:
            if file.endswith(".html") and file not in files_to_keep:
                file_path = os.path.join(folder_path, file)
                os.remove(file_path)
                #print(f"Deleted: {file_path}")

        print("HTML files have been deleted from the folder, except those in the array.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        
def preparePpt(file):
    print("preparePpt")
    import os
    #clean()
    import shutil
    if not os.path.exists("tmp"):
        os.makedirs("tmp")
    
    shutil.copyfile(file,"./tmp/"+file)
    os.chdir("tmp")
    print(os.getcwd())
    
    pdfToHtml(file)
    
    os.remove(file[0:len(file)-4]+".html")
    os.remove(file[0:len(file)-4]+"_ind.html")
    os.remove(file[0:len(file)-4]+"-outline.html")
    
    import replaceImgs as ri
    ri.replaceImgs(".")
    import combineSortedDivs as csd
    csd.extract_sort_and_create_html(".")
    import replace_characters as rc
    rc.replace_nbsp("combined_sorted_divs.html","combined_sorted_divs-clean.html")
    
    os.rename("combined_sorted_divs.html",file[0:len(file)-4]+"_html")
    os.rename("combined_sorted_divs-clean.html",file[0:len(file)-4]+"-clean_html")
    #clean()
    shutil.move(file[0:len(file)-4]+"_html","../"+file[0:len(file)-4]+".html")
    shutil.move(file[0:len(file)-4]+"-clean_html","../"+file[0:len(file)-4]+"-clean.html")
    os.chdir("..")
    shutil.rmtree("tmp")
    
import argparse

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description='A simple command-line argument parser')

# Define command-line arguments
parser.add_argument('-f', '--file', type=str, help='Input file')
parser.add_argument('-o', '--output', type=str, help='Output file')
#parser.add_argument('--flag', action='store_true', help='A boolean flag')

# Parse the command-line arguments
args = parser.parse_args()

# Access the parsed arguments
input_file = args.file

# flag = args.flag

preparePpt(input_file)
