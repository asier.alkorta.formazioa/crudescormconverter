#!/bin/bash

echo "$1"

file="$1"
echo "convert to csv $file"

fCsv1="$file.csv"
echo "processing $fCsv1"
awk -F'\t' 'BEGIN { OFS = "|" } { $1=$1; print $0 }' $file > $fCsv1



fCsv2="$fCsv1-sorted.csv"
echo "Sorting $fCsv1 to $fCsv2"
sort -t '|' -k2 $fCsv1 >$fCsv2

echo "Gather unit data for $fCsv2"
fCsv3="$fCsv2-ldap.csv"
./ldapForEmails.py -i $fCsv2 -o $fCsv3

echo "Sorting "
fCsv4="$fCsv3-sorted.csv"
sort -t '|' -k1 $fCsv3 > $fCsv4


fCsv5="$fCsv4-finalResult.csv"


echo "Joinig $fCsv4 $fCsv2  to $fCsv5"
join -i -a 2 -1 2 -2 1 -t '|' $fCsv2 $fCsv4  > $fCsv5