# Udaletxerako informazioa


## Atera Moodle-etik report-ak

    Joan Ikasgaiak/ikasgai izena/
        Eskiun menuan: Txostenak/laburpena/Txostenak/Laburpena


    Kopiatu txt-ak eta atera amaitu dituztenen emaitzak:

    $ cat input.txt| grep -i "minutu\|@" > input-osatuta.txt

    Eskuz prozesatu eta osatu ez dutenak haseran, eta ondoren bai eskuz konpondu.

    Bihurtu Tsv-ak Csv-etan:
    $ convertTsv2Csv.sh

    Ordenatu
    $ sort -t '|' -k2 data/01-sarrera-es.txt.csv >data/01-sarrera-es.txt-sorted.csv


## Atera Ldap-etik unitateak

    $ ./ldapForEmails.py -i data/01-sarrera-es-osatuta.txt -o tmp/01-sarrera-es.txt-sorted-ldap.csv

    $ sort -t '|' -k1 tmp/01-sarrera-es.txt-sorted-ldap.csv > tmp/01-sarrera-es-osatuta-ldap-sorted.txt



## Konbinatu bi fitxategiak

    Lehenengo ordenatu egin behar dira email eremuaren arabera.

    sort -t '|' -k2 data/01-sarrera-es.txt.csv >data/01-sarrera-es.txt-sorted.csv 
    sort -t ',' -k1 results/01-sarrera-es-osatuta-ldap.txt > results/01-sarrera-es-osatuta-ldap-sorted.txt

    join -a egin 
    join -1 2 -2 2 -t sorted_file1.csv sorted_file2.tsv > output.csv
    
    join -i -a 2 -1 2 -2 1 -t '|' data/01-sarrera-es.txt-sorted.csv  tmp/01-sarrera-es-osatuta-ldap-sorted.txt > output.csv


    awk 'BEGIN {FS="|"} FNR==NR {a[$2]=$0; next} BEGIN {FS=","} {if ($2 in a) print a[$2], $0}' file1.csv file2.tsv


## Prozesatu report-ak

    $ clear && cat results-00-sarrera-e* >result-00-sarrera-osoa.csv 

    $ cat >result-00-sarrera-osoa.csv  | grep "Alcaldia\|EAJ\|BILDU\|Elkarrekin\|PP\|PSOE" | grep -v "UPP">result-xx-udaletxea.csv


    cp data/*finalResult* results/


## Unitate batekoak ateratzeko
    cp data/*finalResult* results/

    cat results/01-sarrera-e* | sort | grep -i "kultura" >results/01-sarrera-Kultura.csv
    

