#!/bin/bash

set folder_path=$argv[1];

for f in data/*.txt; do 
    echo Processing $f
    awk -F'\t' 'BEGIN { OFS = "|" } { $1=$1; print $0 }' $f > $f.csv
done