#!/usr/bin/env python3
import ssl
from ldap3 import Server, Connection, ALL, Tls
import json
import csv

def extractEmails (file_path):
    
    import re

    # Define the regular expression pattern
    pattern= r'([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)' # For email

    # Open the file and read its contents
    with open(file_path, 'r') as file:
        file_contents = file.read()

    # Use the findall method to extract text matching the pattern
    matches = re.findall(pattern, file_contents)

    return matches


def dict_to_csv(data, filename):
    # Extract field names from the first dictionary in the list
    fieldnames = data.keys() if data else []


    with open(filename, 'w') as f:  # You will need 'wb' mode in Python 2.x
        #w = csv.DictWriter(f, data.keys())
        w = csv.writer(f,delimiter='|')
        #w.writeheader()
        w.writerows(data.items())
        #for key in data.keys():
        #    w.writerow(data[key])

def extractUnitsFromEmails(inputFile, result_file):
    # Ficheros de interacción
    
    file_path=inputFile

    # LDAP server settings
    ldap_server = 'ldaps://ldap-lan.donostia.org'
    ldap_user = 'alkzabas@donostia.org'
    ldap_password = '2TMBSKOWOOHstj3t'
    ldap_search_base = 'DC=DONOSTIA,DC=ORG'  # Adjust this to your LDAP structure


    username = 'ALKZABAS'
    email = 'asier_alkorta@donostia.eus'

    ldap_search_by_cn = '(cn=*'+username+'*)'
    ldap_search_by_email = '(mail=*'+email+'*)'

    search_filter = '(objectClass=user)'
    
    print("Extracting emails from source "+file_path+"...")
    emails =extractEmails(file_path)

    print("Searching ldap...")

    # LDAP server settings
    t = Tls(validate=ssl.CERT_NONE)

    print("Creating server...")
    server = Server(ldap_server, use_ssl=True, tls=t, get_info='ALL')# , get_info=Server.GET_ALL

    # SSL certificate validation settings (optional)
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False  # Disable hostname verification
    ssl_context.verify_mode = ssl.CERT_NONE  # Disable certificate validation

    # LDAP connection
    print("Setting connection...")

    conn = Connection(server,ldap_user,ldap_password,auto_bind=True)
    resultDict = {}
    import csv

    for email in emails:
        
        search_filter='(&(objectclass=person)(mail='+email+'))'
        
        conn.search(ldap_search_base,search_filter)
        
        for res in conn.entries:
            
            data = json.loads( res.entry_to_json() )#res.entry_to_json())
            unit= data['dn']
        
            resultDict[email.lower()]= unit
            


    dict_to_csv (resultDict, result_file)




import argparse

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description='A simple command-line argument parser')

# Define command-line arguments
parser.add_argument('-i', '--input', type=str, help='Input file')
parser.add_argument('-o', '--output', type=str, help='Output file')
#parser.add_argument('--flag', action='store_true', help='A boolean flag')

# Parse the command-line arguments
args = parser.parse_args()

# Access the parsed arguments
input = args.input
output = args.output


extractUnitsFromEmails(input, output)
