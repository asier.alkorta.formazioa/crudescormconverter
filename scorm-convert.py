#!/usr/bin/env python3


#pip install BeautifulSoup4

templateFile="00-template-scorm12-init.zip"
scormFolder=templateFile[0:len(templateFile)-4]

manifestElementNamespace="http://www.imsproject.org/xsd/imscp_rootv1p1p2"


def repack(folder):
    import os
    import zipfile

    zf = zipfile.ZipFile(folder+"-repack.zip", "w")
    for dirname, subdirs, files in os.walk(folder):
        zf.write(dirname)
        for filename in files:
            zf.write(os.path.join(dirname, filename))
    zf.close()

def repack2(folder, name):
    import shutil
    shutil.make_archive(name+'-repack-scorm12','zip',folder)



def cleanWorkspace():
    print("Cleaning...")
    try:
        import shutil
        shutil.rmtree(scormFolder)
    except OSError as e:
        print("Error: %s : %s" % (scormFolder, e.strerror))


def prepareWorkspace():

    cleanWorkspace()

    print("Unzipping...")
    import shutil
    shutil.unpack_archive(templateFile, scormFolder, "zip")


def manifest1(fileName,title):

    import xml.etree.ElementTree as ET
    tree = ET.parse(fileName)
    titles = tree.findall('{'+manifestElementNamespace+'}//manifest')
    root =tree.getroot()

    titles =root.findall('.//{'+manifestElementNamespace+'}title')

    for titleNode in titles:
        print(title)
        titleNode.text = title

    ET.register_namespace("", manifestElementNamespace)
    b_xml = ET.tostring(root)
    with open(fileName, "wb") as f:
        f.write(b_xml)


def manifest2(fileName, title):

    from bs4 import BeautifulSoup
    with open(fileName, 'r') as f:
        data = f.read()

    Bs_data = BeautifulSoup(data, "xml")

    b_unique = Bs_data.find_all('title')
    print("number titles: "+str(len(b_unique)))
    for titles in b_unique:
        titles.text = title

def readNotes(zipFileName, fileName):
    import zipfile

    zf = zipfile.ZipFile(zipFileName, 'r')

    for name in zf.namelist():
        if name.endswith('/'): continue
        
        noteFolder='ppt/notesSlides/'
        if  noteFolder+"\\"+fileName in name:
            f = zf.open(name)
            # here you do your magic with [f] : parsing, etc.
            # this will print out file contents
            print(f.read()) 

            import os 
            notes=None
            #if os.path.isfile(name):
            notes=[]
            from bs4 import BeautifulSoup
            with open(fileName, 'r') as f:
                data = f.read()

            Bs_data = BeautifulSoup(data, "xml")

            b_unique = Bs_data.find_all('t')
            print("number note texts: "+str(len(b_unique)))
            for title in b_unique:
                notes.add(title)
    return notes

def manipulateManifest(title):
    #print(title)
    fileName=scormFolder+"/imsmanifest.xml"
    print(str(fileName))
    manifest1(fileName, title)


def convertScorm ():
    print("usage: convertScorm courseName htmlFolder pptxFile scormFolder\n")

    import sys
    n = len(sys.argv)

    courseNames = sys.argv[1].split(",")
    #pptxFile = sys.argv[2].split(",")
    htmlFolders = sys.argv[2].split(",")


    for i in range(0,len(htmlFolders)):
        htmlFolder= htmlFolders[i]
        courseName= courseNames[i]
        
        prepareWorkspace()

        import json
        jsonFileName= scormFolder+'/data/course.json'
        data= None
        with open(jsonFileName, 'r') as f:
            data = json.load(f)

        print ("Json read")
        pageCount = len (data["pages"])
        print ("PageCount "+ str(pageCount))

        scormAssetsFolder = scormFolder+"/assets"

        from os import listdir
        from os.path import isfile, join
        print("Copying assets")
        onlyfiles = [f for f in listdir(htmlFolder) if isfile(join(htmlFolder, f))]
        for f in onlyfiles:

            if f.startswith("img") and f.endswith("png"):
                fIdxStr = f[3:(len(f)-4)]
                fIdx=int(fIdxStr)

                if fIdx>=pageCount:

                    print("\nCopying slide "+f+"...")

                    import shutil
                    destName=  f
                    if fIdx<10:
                        destName="img0"+str(fIdx)+".png"
                    shutil.copyfile(htmlFolder+"/"+f, scormAssetsFolder+"/"+destName)


        sortedfiles = [f for f in listdir(scormAssetsFolder) if isfile(join(scormAssetsFolder+"/", f))]
        for f in sortedfiles:

            if f.startswith("img") and f.endswith("png"):
                fIdxStr = f[3:(len(f)-4)]
                fIdx=int(fIdxStr)

                if fIdx>=pageCount:
                    print("\nAdding slide:",str(fIdx))
                    notesName="notesSlide"+str(fIdx)+".xml"
                    if fIdx<10:
                        imgName="img0"+str(fIdx)+".png"
                    else:
                        imgName="img"+str(fIdx)+".png"

                    dataNode={"title":"Slide "+str(fIdx),"filename": imgName, "order": str(fIdx), "resourceId": "npW0Vp11Ko-"+str(fIdx),"resourceType": "slide","audio": None,"video": None}
                    data["pages"].append(dataNode)
                    # notes =readNotes(pptxFile, notesName)
                    # data["notes"].append(notes)


        #Reset courseId:
        import string
        import random
        ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 10))

        data["courseId"]=ran
        data["name"]=courseName
        print("Writing Json"+jsonFileName+" ...")
        with open(jsonFileName, "w") as file:
            json.dump(data, file)

        manipulateManifest(courseName)
        print("Conversion Done.")
        print("Repacking and cleaning...")

        repack2(scormFolder,courseName )
        cleanWorkspace()
        print("********************************************************************************")
        print("DONE "+courseName+"...")
        print("********************************************************************************")


convertScorm()
