#!/usr/bin/env python3


# pip install python-pptx
from pptx import Presentation
from html import escape

# Load the PPT file
ppt_file = '06_Puesto_trabajo.pptx'
prs = Presentation(ppt_file)

# Initialize an HTML string
html = f'<html><head><title>PPT to HTML</title></head><body>'

# Function to extract and save images
def save_image(image, image_name):
    with open(image_name, 'wb') as img_file:
        img_file.write(image)

# Iterate through slides
for i, slide in enumerate(prs.slides):
    html += f'<div class="slide" style="background-image: url(slide_{i}.jpg); background-size: cover;">'
    for shape in slide.shapes:
        if shape.has_text_frame:
            for paragraph in shape.text_frame.paragraphs:
                for run in paragraph.runs:
                    text = run.text
                    html += f'<p>{escape(text)}</p>'
    html += '</div>'

    # Extract and save the slide background image
    slide_background = slide.background
    if slide_background.fill.solid():
        slide_background_image = slide_background.fill.solid().foreground_image
        if slide_background_image:
            save_image(slide_background_image.blob, f'slide_{i}.jpg')

# Close the HTML tags
html += '</body></html>'

# Save the HTML to a file
with open('output.html', 'w', encoding='utf-8') as html_file:
    html_file.write(html)
