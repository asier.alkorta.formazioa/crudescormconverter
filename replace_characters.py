#!/usr/bin/env python3

# Define the input file and output file
#input_file = "input.txt"
#output_file = "output.txt"

# Function to replace non-breaking space with a regular space
def replace_nbsp_with_space(input_text):
    print("Replacing NonBreaking Spaces...")
    #return input_text.replace(u'\xa0', ' ')
    return input_text.replace(u'\u00A0', ' ')

def replace_nbsp(input_file,output_file):
    try:
        # Open the input file for reading
        with open(input_file, 'r', encoding='utf-8') as infile:
            # Read the content of the input file
            content = infile.read()

            # Replace non-breaking spaces with spaces
            modified_content = replace_nbsp_with_space(content)

            # Open the output file for writing
            with open(output_file, 'w', encoding='utf-8') as outfile:
                # Write the modified content to the output file
                outfile.write(modified_content)

        print("Non-breaking spaces replaced with spaces. Output saved to", output_file)

    except FileNotFoundError:
        print("Input file not found.")
    except Exception as e:
        print("An error occurred:", str(e))
