#!/usr/bin/env python3

import os
from bs4 import BeautifulSoup

# Function to extract <div> content from an HTML file
def extract_div_content(html_file):
    with open(html_file, 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file, 'html.parser')
        div_content = soup.find('div')
        if div_content:
            return str(div_content)
        return None

# Function to create a new HTML file containing all extracted <div> elements
def create_combined_html(output_file, div_contents):
    with open(output_file, 'w', encoding='utf-8') as file:
        file.write('<html>\n<head>\n</head>\n<body>\n')
        for div_content in div_contents:
            file.write(div_content)
        file.write('\n</body>\n</html>')

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        print("Usage: python combine_divs.py /path/to/html_folder")
        sys.exit(1)

    folder_path = sys.argv[1]

    div_contents = []

    # Iterate through HTML files in the folder
    for root, _, files in os.walk(folder_path):
        for file in files:
            if file.lower().endswith('.html'):
                html_file = os.path.join(root, file)
                div_content = extract_div_content(html_file)
                if div_content:
                    div_contents.append(div_content)

    if div_contents:
        output_file = "combined_divs.html"
        create_combined_html(output_file, div_contents)
        print(f"All <div> elements from HTML files have been combined into {output_file}.")
    else:
        print("No <div> elements found in HTML files.")

