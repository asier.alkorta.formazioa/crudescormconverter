



1. Libreoffice ppt-a ireki, eta pdf-a sortu.
2. Kendu hutsuneak pdf-aren izenetik, bestela hurrengo komandoak huts egiten du.
3. pdftohtml erabiliz pdf-a html-ra bihurtu "-c" aukera erabili transformazio konplexuak egiteko.
   $ pdftohtml -c  
4. (Ez da beharrezkoa) Irudi bakoitzaren base64 bihurketa egin.
    https://www.base64-image.de/
    
    $ find . -type f \( -name "*.png" -o -name "*.txt" \) -exec bash -c 'base64 {} > {}.b64' \;

    
5. Ordeztu irudiak base64-engatik
    $ replaceImgs.py  --folder=. --suffix=b64


5. Sortu "Ikasgaia" bat moodle-en. ppt-ko orri bakoitza "eduki orri" bat izango da. ("liburu")
6. Jatorrizko html-ko slide html bakoitzaren div-an kopiatu behar dira moodle-en
7. "Goran Eskuinean "Edukien taula" dago eskuragarri, non atalak gehitu daitezkeen.
8. Bertan WYSIWYG editorean html editorea atera: "<>" 
9. Kopiatu slide-aren "div" edukia.



Moodle-en url-a:
    https://intranet.donostia.eus/moodle
    



# Gidalerroak?
        
    D:\Garapena\ENS_2022\kontzienziazio_webgunea_05\Temas\Temario anual\zz_Moodle\Moodle-Egitura.odt
    
    Ikastaroa sortu zenean egitura sortu zen.








Prompts


can you make a python script that gets all the html, and  png  files in a folder, and for each html file opens it and searches in its content to find the img tag (there's always only one), and replaces the name of the png file in the src tag, for the content of the b64 file for such png file


