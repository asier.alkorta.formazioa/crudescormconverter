#!/usr/bin/env python3
import os
import re
from bs4 import BeautifulSoup

# Custom sorting function for <div> elements based on their 'id' attribute
def custom_div_sort(div):
    # Extract the 'X' value from the 'id' attribute
    div_id = div.get('id')
    match = re.search(r'page(\d+)-div', div_id)
    if match:
        return int(match.group(1))
    else:
        return float('inf')  # Use a large value for invalid 'id' attributes

# Function to extract <div> content, sort them, and create a new HTML file
def extract_sort_and_create_html(folder_path):
    sorted_divs = []
    
    # Iterate through HTML files in the folder
    for root, _, files in os.walk(folder_path):
        for file in files:
            if file.lower().endswith('.html'):
                html_file = os.path.join(root, file)
                with open(html_file, 'r', encoding='utf-8') as file:
                    soup = BeautifulSoup(file, 'html.parser')
                    divs = soup.find_all('div', id=re.compile(r'page\d+-div'))
                    sorted_divs.extend(divs)

    if sorted_divs:
        sorted_divs.sort(key=custom_div_sort)
        
        # Create a new HTML file containing all the sorted <div> elements
        output_file = "combined_sorted_divs.html"
        with open(output_file, 'w', encoding='utf-8') as file:
            file.write('<html>\n<head>\n</head>\n<body>\n')
            for div in sorted_divs:
                file.write(str(div))
            file.write('\n</body>\n</html>')
        
        print(f"All <div> elements from HTML files have been combined and sorted into {output_file}.")
    else:
        print("No <div> elements found in HTML files.")

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        print("Usage: python sort_and_combine_divs.py /path/to/html_folder")
        sys.exit(1)

    folder_path = sys.argv[1]
    extract_sort_and_create_html(folder_path)
