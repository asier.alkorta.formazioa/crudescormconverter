#!/usr/bin/env python3


import os
import re
import base64

print("Replacing images ")

# Directory where the HTML and PNG files are located
#folder_path = '.'

# Function to replace the src attribute in an HTML file with b64 content
def replace_img_src(html_file, png_file, suffix):
    #print("Replace images for:"+html_file)
    with open(html_file, 'r', encoding='utf-8') as file:
        html_content = file.read()

    # Use regex to find the img tag
    img_tag = re.search(r'<img\s+.*?src=["\'](.*?\.png)["\']', html_content)
    if img_tag:
        png_file_name = img_tag.group(1)
        
        if os.path.exists(png_file_name):
            # Read the content of the corresponding PNG file
            with open(png_file_name, 'rb') as png_file:
                png_content = png_file.read()
            
            # Encode the PNG content in base64
            b64_encoded = base64.b64encode(png_content).decode('utf-8')
            
            # Replace the src attribute with the base64-encoded content
            updated_html = re.sub(
                r'<img\s+.*?src=["\'](.*?\.png)["\']',
                f'<img src="data:image/png;base64,{b64_encoded}"',
                html_content
            )

            # Write the updated content back to the HTML file
            newName=html_file[0: (len(html_file)-4)]+suffix+".html"
            #print(newName)
            with open (newName, 'w', encoding='utf-8') as file:
                file.write(updated_html)
        else:
            print(f"PNG file '{png_file_name}' not found for HTML file '{html_file}'")


def replaceImgs(folder_path, suffix):
    #print("Replacing imgs for their base64 code in folder:"+folder_path)
    # Iterate through HTML and PNG files
    from tqdm import tqdm
    for root, _, files in os.walk(folder_path):
        for file in tqdm(files):
            file_path = os.path.join(root, file)

            if file.lower().endswith('.html'):
                # Process HTML file
                # print(f"Processing HTML file: {file}")
                for png_file in files:
                    if png_file.lower().endswith('.png'):
                        replace_img_src(file_path, os.path.join(root, png_file), suffix)




import argparse

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description='A simple command-line argument parser')

# Define command-line arguments
parser.add_argument('-f', '--folder', type=str, help='Input file')
parser.add_argument('-s', '--suffix', type=str, help='Output file')
#parser.add_argument('--flag', action='store_true', help='A boolean flag')

# Parse the command-line arguments
args = parser.parse_args()

# Access the parsed arguments
folder = args.folder
suffix = args.suffix

# flag = args.flag

print("Replacing images for folder:"+folder+" with suffix: "+suffix)
replaceImgs(folder, suffix)

